/*
 *
 *  When: 2016/09/03
 *  Who: Zheng Shou
 *  Where: Columbia University
 *
 */


#ifndef SWAP_BLOB_ARRAY_CHANNEL_LENGTH_HPP_
#define SWAP_BLOB_ARRAY_CHANNEL_LENGTH_HPP_


namespace caffe {

template <typename Dtype>
void swap_blob_array_channel_length_cpu(const Dtype* data_in, const int channels, const int length,
    const int height, const int width, Dtype* data_out);

template <typename Dtype>
void swap_blob_array_channel_length_gpu(const Dtype* data_in, const int channels, const int length,
	    const int height, const int width, Dtype* data_out);
}  // namespace caffe


#endif /* SWAP_BLOB_ARRAY_CHANNEL_LENGTH_HPP_ */
