/*
 *
 *  Copyright (c) 2016, Facebook, Inc. All rights reserved.
 *
 *  Licensed under the Creative Commons Attribution-NonCommercial 3.0
 *  License (the "License"). You may obtain a copy of the License at
 *  https://creativecommons.org/licenses/by-nc/3.0/.
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 *  License for the specific language governing permissions and limitations
 *  under the License.
 *
 */




#include <algorithm>
#include <cfloat>
#include <vector>

#include "caffe/layer.hpp"
#include "caffe/frame_wise_softmax_loss_binary_layer.hpp"
#include "caffe/util/math_functions.hpp"

using std::max;
using std::count;
using std::max_element;
using std::distance;

namespace caffe {

template <typename Dtype>
void FrameWiseSoftmaxWithLossBinaryLayer<Dtype>::SetUp(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top) {
  CHECK_EQ(bottom.size(), 2) << "SoftmaxLoss Layer takes two blobs as input.";
  CHECK_LE(top->size(), 1) << "SoftmaxLoss Layer takes at most 1 blob as output.";	// 20160905

  if (top->size()==1) {(*top)[0]->Reshape(1, 1, 1, 1, 1);}	// 20160905
  
  softmax_bottom_vec_.clear();
  softmax_bottom_vec_.push_back(bottom[0]);
  softmax_top_vec_.push_back(&prob_);
  softmax_layer_->SetUp(softmax_bottom_vec_, &softmax_top_vec_);
  
  interp_rate_t_ = this->layer_param_.frame_softmax_loss_param().interp_rate_t();	// 20160821
}

template <typename Dtype>
Dtype FrameWiseSoftmaxWithLossBinaryLayer<Dtype>::Forward_cpu(
    const vector<Blob<Dtype>*>& bottom, vector<Blob<Dtype>*>* top) {

  // The forward pass computes the softmax prob values.
  softmax_bottom_vec_[0] = bottom[0];
  softmax_layer_->Forward(softmax_bottom_vec_, &softmax_top_vec_);

  const Dtype* prob_data = prob_.cpu_data();
  const Dtype* truth = bottom[1]->cpu_data();

  // 20160821
  const int num = bottom[0]->num();
  const int channels = bottom[0]->channels();
  const int length = bottom[0]->length();
  const int truth_length = bottom[1]->length();
  CHECK_EQ(interp_rate_t_, truth_length/length);

  Dtype loss = 0;
  Dtype accuracy = 0;	// 20160905
  int num_of_class = 22;	// 20160919

  for (int i = 0; i < num; ++i) {	// i c l h w all index starting from 0
	  for (int l = 0; l < length; l++) {
				  // int label = truth[bottom[1]->offset(i,0,l,h,w)];
				  /*
				  Dtype label_sum = 0;
				  for (int idx = interp_rate_t_*l; idx < interp_rate_t_*(l+1); idx++) { label_sum = label_sum + truth[bottom[1]->offset(i,0,idx,0,0)]; }
				  int label = round(label_sum/interp_rate_t_);
				  */
				  
				  vector<Dtype> slabel;
				  for (int idx = interp_rate_t_*l; idx < interp_rate_t_*(l+1); idx++) { slabel.push_back(truth[bottom[1]->offset(i,0,idx,0,0)]); }
				  
				  // 20160919 start
			      int counts[num_of_class];
				  for (int idx=0; idx<num_of_class; idx++) { counts[idx]=count(slabel.begin(), slabel.end(), idx); }
				  int label = distance(counts, max_element(counts, counts+num_of_class));
				  // LOG(INFO) << "label: " << label; // 20160919
				  if (label > 0) {
					  if (label == num_of_class - 1) {	label = channels - 1; }
					  else {	label = 1; }
				  }
				  // 20160919 end
				  
				  if (label < channels - 1) {	// 20160821 take into account ambiguous frame
					  
					  loss += -log(max(prob_data[bottom[0]->offset(i,label,l,0,0)],
		                     Dtype(FLT_MIN)));
							 
					  if (top->size()==1) { 	// 20160905 compute accuracy
					        Dtype maxval = -FLT_MAX;
							int max_id = 0;
							for (int j = 0; j < channels; ++j) {
							  if (prob_data[bottom[0]->offset(i,j,l,0,0)] > maxval) {
								maxval = prob_data[bottom[0]->offset(i,j,l,0,0)];
								max_id = j;
							  }
							}
							 // LOG(INFO) << "label: " << label; // 20160906
							 // LOG(INFO) << "max_id: " << max_id; // 20160906
							if ( label == max_id ) { accuracy += 1; } 
						}
				  }
			  }
  }
    
  if (top->size()==1) {
	  (*top)[0]->mutable_cpu_data()[0] = accuracy / (num * length);
	  // LOG(INFO) << "accuracy: " << accuracy / (num * length);
	  }	// 20160905
  
  LOG(INFO) << "binary loss: " << loss / (num * length);
  return loss / (num * length);
}

template <typename Dtype>
void FrameWiseSoftmaxWithLossBinaryLayer<Dtype>::Backward_cpu(const vector<Blob<Dtype>*>& top,
    const bool propagate_down,
    vector<Blob<Dtype>*>* bottom) {
  // Compute the diff
  Dtype* bottom_diff = (*bottom)[0]->mutable_cpu_diff();
  const Dtype* prob_data = prob_.cpu_data();
  memcpy(bottom_diff, prob_data, sizeof(Dtype) * prob_.count());

  const Dtype* truth = (*bottom)[1]->cpu_data();
  
  // 20160821
  const int num = (*bottom)[0]->num();
  const int channels = (*bottom)[0]->channels();
  const int length = (*bottom)[0]->length();
  const int truth_length = (*bottom)[1]->length();
  CHECK_EQ(interp_rate_t_, truth_length/length);
  
  int num_of_class = 22;	// 20160919

  for (int i = 0; i < num; ++i) {
	  for (int l = 0; l < length; l++) {
				  // int label = truth[(*bottom)[1]->offset(i,0,l,h,w)];
				  
				  /*
				  Dtype label_sum = 0;
				  for (int idx = interp_rate_t_*l; idx < interp_rate_t_*(l+1); idx++) { label_sum = label_sum + truth[(*bottom)[1]->offset(i,0,idx,0,0)]; }
				  int label = round(label_sum/interp_rate_t_);
				  */
				  
				  vector<Dtype> slabel;
				  for (int idx = interp_rate_t_*l; idx < interp_rate_t_*(l+1); idx++) { slabel.push_back(truth[(*bottom)[1]->offset(i,0,idx,0,0)]); }
				  
				  // 20160919 start
			      int counts[num_of_class];
				  for (int idx=0; idx<num_of_class; idx++) { counts[idx]=count(slabel.begin(), slabel.end(), idx); }
				  int label = distance(counts, max_element(counts, counts+num_of_class));
				  if (label > 0) {
					  if (label == num_of_class - 1) {	label = channels - 1; }
					  else {	label = 1; }
				  }
				  // 20160919 end
				  
				  if (label < channels - 1) {	// 20160821 take into account ambiguous frame
					  bottom_diff[(*bottom)[0]->offset(i,label,l,0,0)] -= 1;
				  }
			  }
  }

  // Scale down gradient
  caffe_scal(prob_.count(), Dtype(1) / (num * length), bottom_diff);
}


INSTANTIATE_CLASS(FrameWiseSoftmaxWithLossBinaryLayer);


}  // namespace caffe
